export const links = [
  {
    name: "Basic Bullets",
    web: "BasicBullets",
  },
  {
    name: "Basic Collision Detection",
    web: "BasicCollisionDetection",
  },
  {
    name: "Containers",
    web: "Containers",
  },
  {
    name: "CSS Fonts",
    web: "CSSFonts",
  },
  {
    name: "Follow Mouse",
    web: "FollowMouse",
  },
  {
    name: "Keyboard Movement",
    web: "KeyboardMovement",
  },
  {
    name: "Preloading Assets",
    web: "PreloadingAssets",
  },
  {
    name: "Rodar",
    web: "Rodar",
  },
  {
    name: "Shoot Em Up",
    web: "ShootEmUp",
  },
  {
    name: "Spine",
    web: "Spine",
  },
  {
    name: "SpriteSheet",
    web: "SpriteSheet",
  },
  {
    name: "Wheel Of Fortune",
    web: "WheelOfFortune",
  },
];

export const externs = [];
