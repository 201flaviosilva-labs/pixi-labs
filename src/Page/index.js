import Assets from "../Assets.js";
import { randomColor, deleteAllChildDom } from "../utils.js";
import { links, externs } from "../link.js";

const basePath = "./src/Labs/";

// Dom
document.getElementById("title").style.color = randomColor();
const ulList = document.getElementById("ulList");

function main() {
  deleteAllChildDom(ulList);

  links.map(link => {
    printLink(link);
  });

  externs.map(link => {
    printLink(link);
  });
}

function printLink(link) {
  const li = document.createElement("li");
  const gameName = document.createElement("h3");
  const WebLink = document.createElement("a");
  const WebImg = document.createElement("img");

  li.style.borderColor = "white";
  li.style.background = "orange";

  li.title = link.name;
  gameName.innerHTML = link.name;

  const divLinks = document.createElement("div");
  divLinks.classList.add("divLinks");

  WebLink.href = basePath + link.web + "/index.html";
  WebImg.src = Assets.Icons.Web;

  WebLink.appendChild(WebImg);
  divLinks.appendChild(WebLink);

  li.appendChild(gameName);
  li.appendChild(divLinks);
  ulList.appendChild(li);
}

main();
