import Assets from "../../Assets.js";

class Game {
	constructor() {
		console.clear();
		this.configs = {
			width: 500,
			height: 500,
			resolution: window.devicePixelRatio || 1,
			antialias: true,
		};
		this.app = new PIXI.Application(this.configs);
		this.mainContainer = new PIXI.Container();
		this.app.stage.addChild(this.mainContainer);

		document.body.appendChild(this.app.view);

		this.setup();
	}

	async setup() {
		this.createStats();
		this.init();
		await this.preload();
		this.create();
		this.update();
	}

	init() {
		this.configs.middleWidth = this.configs.width / 2;
		this.configs.middleHeight = this.configs.height / 2;

		this.app.stage.interactive = true;

		this.player;
	}

	createStats() {
		this.stats = new Stats();
		this.stats.showPanel(0); // 0: fps, 1: ms, 2: mb, 3+: custom
		this.stats.domElement.style.position = "fixed";
		this.stats.domElement.style.left = "0px";
		this.stats.domElement.style.top = "0px";
		document.body.appendChild(this.stats.domElement);
	}

	async preload() { }

	create() {
		const { width, height, middleWidth, middleHeight } = this.configs;
		this.player = new PIXI.Sprite.from(Assets.Sprites.Quadrados.Verde);
		this.player.anchor.set(0.5);
		this.player.position.set(middleWidth, middleHeight);
		this.mainContainer.addChild(this.player);
		this.app.stage.on("pointermove", e => this.player.position.set(e.data.global.x, e.data.global.y));
	}

	update() {
		this.stats.update();
		requestAnimationFrame(this.update.bind(this));
	}
}

const game = new Game();
