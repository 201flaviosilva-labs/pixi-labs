import Assets from "../../Assets.js";

class Game {
	constructor() {
		console.clear();
		this.configs = {
			width: 500,
			height: 500,
			resolution: window.devicePixelRatio || 1,
			antialias: true,
		};
		this.app = new PIXI.Application(this.configs);
		this.mainContainer = new PIXI.Container();
		this.app.stage.addChild(this.mainContainer);

		this.domGameContainer = document.getElementById("gameContainer");
		this.domGameContainer.appendChild(this.app.view);

		this.setup();
	}

	setup() {
		this.createStats();
		this.init();
		this.preload();
	}
	loadPreload() {
		this.create();
		this.app.ticker.add(this.update.bind(this));
	}

	createStats() {
		this.stats = new Stats();
		this.stats.showPanel(0); // 0: fps, 1: ms, 2: mb, 3+: custom
		this.stats.domElement.style.position = "fixed";
		this.stats.domElement.style.left = "0px";
		this.stats.domElement.style.top = "0px";
		document.body.appendChild(this.stats.domElement);
	}

	init() {
		this.configs.middleWidth = this.configs.width / 2;
		this.configs.middleHeight = this.configs.height / 2;

		this.app.stage.interactive = true;

		this.player;
		this.bullets = [];
		this.bulletSpeed = 10;
	}

	preload() {
		const { Amarelo, Verde, Vermelho } = Assets.Sprites.Quadrados;

		this.app.loader
			.add("Verde", Verde)
			.add("Shoot", Assets.Sprites.Disparo.Flame);

		this.app.loader.onComplete.add((e) => this.loadPreload());
		this.app.loader.load();
	}

	create() {
		const { width, height, middleWidth, middleHeight } = this.configs;
		this.player = new PIXI.Sprite.from(this.app.loader.resources.Verde.texture);
		this.player.anchor.set(0.5);
		this.player.position.set(middleWidth, middleHeight);
		this.mainContainer.addChild(this.player);
		this.app.stage.on("pointermove", e => this.player.position.set(e.data.global.x, e.data.global.y));
		this.domGameContainer.addEventListener("pointerup", e => this.fire());
	}

	fire() {
		const bullet = new PIXI.Sprite.from(this.app.loader.resources["Shoot"].texture);
		bullet.anchor.set(0.5);
		bullet.scale.set(0.5);
		bullet.position.set(this.player.x, this.player.y);
		this.mainContainer.addChild(bullet);
		this.bullets.push(bullet);
	}

	update() {
		this.moveBullets();
		this.stats.update();
	}

	moveBullets() {
		this.bullets.forEach((bullet, i) => {
			bullet.y -= this.bulletSpeed;
			if (bullet.y < 0) {
				this.mainContainer.removeChild(bullet);
				this.bullets.splice(i, 1);
			}
		});
	}
}

const game = new Game();
