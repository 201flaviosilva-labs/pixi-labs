import Assets from "../../Assets.js";

class Game {
	constructor() {
		console.clear();
		this.configs = {
			width: 500,
			height: 500,
			antialias: true,
		};
		this.app = new PIXI.Application(this.configs);
		this.mainContainer = new PIXI.Container();
		this.app.stage.addChild(this.mainContainer);

		document.body.appendChild(this.app.view);

		this.setup();
	}

	async setup() {
		this.createStats();
		this.init();
		await this.preload();
		this.create();
		this.app.ticker.add(this.update.bind(this));
	}

	createStats() {
		this.stats = new Stats();
		this.stats.showPanel(0);
		this.stats.domElement.style.position = "fixed";
		this.stats.domElement.style.left = "0px";
		this.stats.domElement.style.top = "0px";
		document.body.appendChild(this.stats.domElement);
	}

	init() {
		this.configs.middleWidth = this.configs.width / 2;
		this.configs.middleHeight = this.configs.height / 2;

		this.player;
		this.speed = 1;

		this.moveKeys = [];
	}

	async preload() { }

	create() {
		const { width, height, middleWidth, middleHeight } = this.configs;
		this.player = new PIXI.Sprite.from(Assets.Sprites.Quadrados.Verde);
		this.player.anchor.set(0.5);
		this.player.position.set(middleWidth, middleHeight);
		this.mainContainer.addChild(this.player);

		// Keyboard
		window.addEventListener("keydown", this.onKeyDown.bind(this));
		window.addEventListener("keyup", this.onKeyUp.bind(this));
	}

	onKeyDown(e) {
		this.moveKeys[e.keyCode] = true;
	}

	onKeyUp(e) {
		this.moveKeys[e.keyCode] = false;
	}

	update() {
		this.stats.update();

		if (this.moveKeys["87"] || this.moveKeys["38"]) this.player.y -= this.speed; // Up
		if (this.moveKeys["83"] || this.moveKeys["40"]) this.player.y += this.speed; // Down
		if (this.moveKeys["65"] || this.moveKeys["37"]) this.player.x -= this.speed; // Left
		if (this.moveKeys["68"] || this.moveKeys["39"]) this.player.x += this.speed; // Right
	}
}

const game = new Game();
