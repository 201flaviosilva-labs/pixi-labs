import Assets from "../../Assets.js";

class Game {
	constructor() {
		console.clear();
		this.configs = {
			width: 500,
			height: 500,
			antialias: true,
		};
		this.app = new PIXI.Application(this.configs);
		this.mainContainer = new PIXI.Container();
		this.app.stage.addChild(this.mainContainer);

		document.body.appendChild(this.app.view);

		this.setup();
	}

	setup() {
		this.createStats();
		this.init();
		this.preload();
	}

	posPreload() {
		this.create();
		this.app.ticker.add(this.update.bind(this));
	}

	createStats() {
		this.stats = new Stats();
		this.stats.showPanel(0);
		this.stats.domElement.style.position = "fixed";
		this.stats.domElement.style.left = "0px";
		this.stats.domElement.style.top = "0px";
		document.body.appendChild(this.stats.domElement);
	}

	init() {
		this.configs.middleWidth = this.configs.width / 2;
		this.configs.middleHeight = this.configs.height / 2;


		this.speed = 1;
		this.playerAnimation = {
			size: Assets.Sprites.Dude.size,
			animations: {
				idle: [],
				left: [],
				right: [],
				idleLeft: [],
				idleRight: [],
			},
		};
		this.moveKeys = [];
	}

	preload() {
		this.app.loader
			.add("Dude", Assets.Sprites.Dude.png);

		this.app.loader.onComplete.add((e) => this.posPreload());

		this.app.loader.load();
	}

	create() {
		const { width, height, middleWidth, middleHeight } = this.configs;
		const texture = new PIXI.BaseTexture.from(this.app.loader.resources["Dude"].url);

		// Create Texture
		const { frameWidth, frameHeight } = this.playerAnimation.size;

		this.playerAnimation.animations.idleLeft = [new PIXI.Texture(texture, new PIXI.Rectangle(0 * frameWidth, 0, frameWidth, frameHeight))];
		this.playerAnimation.animations.idleRight = [new PIXI.Texture(texture, new PIXI.Rectangle(5 * frameWidth, 0, frameWidth, frameHeight))];
		this.playerAnimation.animations.left = [
			new PIXI.Texture(texture, new PIXI.Rectangle(0 * frameWidth, 0, frameWidth, frameHeight)),
			new PIXI.Texture(texture, new PIXI.Rectangle(1 * frameWidth, 0, frameWidth, frameHeight)),
			new PIXI.Texture(texture, new PIXI.Rectangle(2 * frameWidth, 0, frameWidth, frameHeight)),
			new PIXI.Texture(texture, new PIXI.Rectangle(3 * frameWidth, 0, frameWidth, frameHeight)),
		];
		this.playerAnimation.animations.idle = [
			new PIXI.Texture(texture, new PIXI.Rectangle(4 * frameWidth, 0, frameWidth, frameHeight)),
		];
		this.playerAnimation.animations.right = [
			new PIXI.Texture(texture, new PIXI.Rectangle(5 * frameWidth, 0, frameWidth, frameHeight)),
			new PIXI.Texture(texture, new PIXI.Rectangle(6 * frameWidth, 0, frameWidth, frameHeight)),
			new PIXI.Texture(texture, new PIXI.Rectangle(7 * frameWidth, 0, frameWidth, frameHeight)),
			new PIXI.Texture(texture, new PIXI.Rectangle(8 * frameWidth, 0, frameWidth, frameHeight)),
		];

		// Create Player
		this.player = new PIXI.AnimatedSprite(this.playerAnimation.animations.idle);
		this.player.animationSpeed = 0.1;
		this.player.loop = false;
		this.player.play();
		this.player.position.set(middleWidth, middleHeight);
		this.player.anchor.set(0.5);
		this.player.scale.set(2);
		this.mainContainer.addChild(this.player);

		// Keyboard
		window.addEventListener("keydown", this.onKeyDown.bind(this));
		window.addEventListener("keyup", this.onKeyUp.bind(this));
	}

	onKeyDown(e) {
		this.moveKeys[e.keyCode] = true;
	}

	onKeyUp(e) {
		this.moveKeys[e.keyCode] = false;
	}

	update() {
		this.stats.update();

		if (this.moveKeys["65"] || this.moveKeys["37"]) {// Left
			this.player.x -= this.speed;
			if (!this.player.playing) {
				this.player.textures = this.playerAnimation.animations.left;
				this.player.play();
			}
		} else if (this.moveKeys["68"] || this.moveKeys["39"]) {// Right
			this.player.x += this.speed;
			if (!this.player.playing) {
				this.player.textures = this.playerAnimation.animations.right;
				this.player.play();
			}
		} else { // Idle
			this.player.textures = this.playerAnimation.animations.idle;
		}
	}
}

const game = new Game();
