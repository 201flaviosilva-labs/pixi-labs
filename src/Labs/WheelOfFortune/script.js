import Assets from "../../Assets.js";
import { degreesToRadians, randomColor0X, randomNumber } from "../../utils.js";

class Game {
	constructor() {
		this.screen = {
			width: 500,
			height: 500,
			middleWidth: 0,
			middleHeight: 0,
		};
		this.screen.middleWidth = this.screen.width / 2;
		this.screen.middleHeight = this.screen.height / 2;

		this.StatusFPSs = document.getElementById("StatusFPSs");
		this.app = new PIXI.Application(this.screen.width, this.screen.height, { antialias: true, transparent: false, resolution: 1 });
		this.stage = new PIXI.Container();

		this.app.renderer.backgroundColor = 0xffffff;
		document.body.appendChild(this.app.view);

		this.setup();
	}

	setup() {
		this.init();
		this.preload();
		this.create();
		this.update();
	}

	init() {
		this.numSlices = 10;
		this.radius = this.screen.middleHeight - 5;
		this.radBySlice = Math.PI * 2 / this.numSlices;
	}

	preload() { }

	create() {
		const { width, height, middleWidth, middleHeight } = this.screen;

		{ // Wheel
			this.wheel = new PIXI.Container();
			// this.wheel.anchor.set(0.5, 0.5);
			this.wheel.pivot.set(middleWidth, middleHeight);
			this.wheel.position.set(middleWidth, middleHeight);
			this.wheel.buttonMode = true;
			this.wheel.interactive = true;

			let spinning = false;
			this.wheel.on("pointertap", () => {
				if (spinning) return;
				this.wheel.rotation = 0;
				spinning = true;
				const duration = randomNumber(200, 5000);
				const rotation = degreesToRadians(randomNumber(720, 3600));
				anime({
					targets: this.wheel,
					easing: "easeOutQuint",
					duration,
					rotation,
					complete: () => { spinning = false; },
				});
			});

			{ // Slice Graphics
				const graphic = new PIXI.Graphics();
				graphic.position.set(middleWidth, middleHeight);
				graphic.lineStyle(1, 0x000000, 0.5);

				for (let i = 0; i < this.numSlices; i++) {
					const startAngle = i * this.radBySlice - this.radBySlice / 2;
					const endAngle = startAngle + this.radBySlice;
					const color = randomColor0X();

					graphic.moveTo(0, 0);
					graphic.beginFill(color);
					graphic.arc(0, 0, this.radius, startAngle, endAngle);
				};

				this.wheel.addChild(graphic);
			}

			this.app.stage.addChild(this.wheel);
		}
	}

	update() {
		this.StatusFPSs.innerHTML = Number(PIXI.ticker.shared.FPS).toFixed(2);
		requestAnimationFrame(this.update.bind(this));
	}
}

const game = new Game();
