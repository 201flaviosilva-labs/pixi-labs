import Assets from "../../Assets.js";

class Game {
	constructor() {
		console.clear();
		this.configs = {
			width: 500,
			height: 500,
			antialias: true,
		};
		this.app = new PIXI.Application(this.configs);
		this.mainContainer = new PIXI.Container();
		this.app.stage.addChild(this.mainContainer);

		document.body.appendChild(this.app.view);

		this.setup();
	}

	setup() {
		this.createStats();
		this.init();
		this.preload();
	}

	posPreload() {
		this.create();
		this.app.ticker.add(this.update.bind(this));
	}

	createStats() {
		this.stats = new Stats();
		this.stats.showPanel(0);
		this.stats.domElement.style.position = "fixed";
		this.stats.domElement.style.left = "0px";
		this.stats.domElement.style.top = "0px";
		document.body.appendChild(this.stats.domElement);
	}

	init() {
		this.configs.middleWidth = this.configs.width / 2;
		this.configs.middleHeight = this.configs.height / 2;
	}

	preload() {
		this.app.loader
			.add("SpineBoy", Assets.PhaserLabs.spine["3.8"].spineBoy.pro.json);

		this.app.loader.load();
		this.app.loader.onComplete.add((e) => this.posPreload());
	}

	create() {
		const { width, height, middleWidth, middleHeight } = this.configs;

		const spineBoy = new PIXI.spine.Spine(this.app.loader.resources.SpineBoy.spineData);
		spineBoy.state.setAnimation(0, "idle", true); // idle

		spineBoy.position.set(middleWidth, height);
		spineBoy.scale.set(0.5);

		this.mainContainer.addChild(spineBoy);

		const labelStyle = {
			fontFamily: "Arial",
			fontSize: 20,
			fill: 0xffffff,
			align: "left",
		};
		const animationsKeys = Object.keys(this.app.loader.resources.SpineBoy.data.animations);
		animationsKeys.forEach((key, i) => {
			const label = new PIXI.Text(`${i + 1} - ${key}`, labelStyle);
			label.position.set(10, i * 24 + 10);
			label.interactive = true;
			label.buttonMode = true;
			label.on("pointerdown", () => spineBoy.state.setAnimation(0, key, true));
			this.mainContainer.addChild(label);
		});
	}

	update() {
		this.stats.update();
	}
}

const game = new Game();
