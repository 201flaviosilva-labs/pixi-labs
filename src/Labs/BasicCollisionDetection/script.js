import Assets from "../../Assets.js";

class Game {
	constructor() {
		console.clear();
		this.configs = {
			width: 500,
			height: 500,
			resolution: window.devicePixelRatio || 1,
			antialias: true,
		};
		this.app = new PIXI.Application(this.configs);
		this.mainContainer = new PIXI.Container();
		this.app.stage.addChild(this.mainContainer);

		this.domGameContainer = document.getElementById("gameContainer");
		this.domGameContainer.appendChild(this.app.view);

		this.setup();
	}

	setup() {
		this.createStats();
		this.init();
		this.preload();
	}
	loadPreload() {
		this.create();
		this.app.ticker.add(this.update.bind(this));
	}

	createStats() {
		this.stats = new Stats();
		this.stats.showPanel(0);
		this.stats.domElement.style.position = "fixed";
		this.stats.domElement.style.left = "0px";
		this.stats.domElement.style.top = "0px";
		document.body.appendChild(this.stats.domElement);
	}

	init() {
		this.configs.middleWidth = this.configs.width / 2;
		this.configs.middleHeight = this.configs.height / 2;

		this.app.stage.interactive = true;
	}

	preload() {
		const { Amarelo, Verde, Vermelho } = Assets.Sprites.Quadrados;

		this.app.loader
			.add("Vermelho", Vermelho)
			.add("Verde", Verde)
			.add("Amarelo", Amarelo)
			.add("Shoot", Assets.Sprites.Disparo.Flame);

		this.app.loader.onComplete.add((e) => this.loadPreload());
		this.app.loader.load();
	}

	create() {
		const { width, height, middleWidth, middleHeight } = this.configs;

		// Player
		this.player = new PIXI.Sprite.from(this.app.loader.resources.Verde.texture);
		this.player.anchor.set(0.5);
		this.player.position.set(middleWidth, middleHeight);
		this.mainContainer.addChild(this.player);
		this.app.stage.on("pointermove", e => this.player.position.set(e.data.global.x, e.data.global.y));

		// Enemy
		this.enemy = new PIXI.Sprite.from(this.app.loader.resources.Vermelho.texture);
		this.enemy.anchor.set(0.5);
		this.enemy.position.set(middleWidth, 100);
		this.mainContainer.addChild(this.enemy);
	}

	update() {
		if (this.checkRectangleCollision(this.player, this.enemy)) {
			console.log("Collision");
			this.player.alpha = 0.5;
			this.enemy.alpha = 0.5;
		}
		this.stats.update();
	}

	checkRectangleCollision(r1, r2) {
		const r1Bounds = r1.getBounds();
		const r2Bounds = r2.getBounds();

		return !(
			r2Bounds.x > r1Bounds.x + r1Bounds.width ||
			r2Bounds.x + r2Bounds.width < r1Bounds.x ||
			r2Bounds.y > r1Bounds.y + r1Bounds.height ||
			r2Bounds.y + r2Bounds.height < r1Bounds.y
		);
	}
}

const game = new Game();
