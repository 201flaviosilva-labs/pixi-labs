import Assets from "../../Assets.js";

class Game {
	constructor() {
		console.clear();
		this.configs = {
			width: 500,
			height: 500,
			antialias: true,
		};
		this.app = new PIXI.Application(this.configs);
		this.mainContainer = new PIXI.Container();
		this.app.stage.addChild(this.mainContainer);

		document.body.appendChild(this.app.view);

		this.setup();
	}

	setup() {
		this.createStats();
		this.init();
		this.preload();
	}

	posPreload() {
		this.create();
		this.app.ticker.add(this.update.bind(this));
	}

	createStats() {
		this.stats = new Stats();
		this.stats.showPanel(0);
		this.stats.domElement.style.position = "fixed";
		this.stats.domElement.style.left = "0px";
		this.stats.domElement.style.top = "0px";
		document.body.appendChild(this.stats.domElement);
	}

	init() {
		this.configs.middleWidth = this.configs.width / 2;
		this.configs.middleHeight = this.configs.height / 2;
	}

	preload() {
		this.app.loader
			.add("Ninica1", Assets.Images.Ninica.Ninica1)
			.add("Ninica2", Assets.Images.Ninica.Ninica2)
			.add("Ninica3", Assets.Images.Ninica.Ninica3);

		this.app.loader.onComplete.add((e) => this.posPreload());

		this.app.loader.load();
	}

	create() {
		const { width, height, middleWidth, middleHeight } = this.configs;
		const textStyle = {
			fontSize: 40,
			fontFamily: "Press Start 2P",
			fill: 0xffffff,
			fontWeight: "bold",
			alignment: "center",
			strokeThickness: 10,
		};

		// Container 1
		const container1 = new PIXI.Container();

		const bg1 = new PIXI.Graphics();
		bg1.beginFill(0xff0000);
		bg1.drawRect(0, 0, width, height);
		bg1.endFill();
		container1.addChild(bg1);

		const ninica1 = new PIXI.Sprite(this.app.loader.resources["Ninica1"].texture);
		ninica1.position.set(middleWidth, middleHeight);
		ninica1.anchor.set(0.5);
		ninica1.scale.set(0.25);
		container1.addChild(ninica1);

		const text1 = new PIXI.Text("Ninica 1", textStyle);
		text1.position.set(middleWidth, middleHeight);
		text1.anchor.set(0.5);
		container1.addChild(text1);

		this.mainContainer.addChild(container1);

		// Container 2
		const container2 = new PIXI.Container();
		container2.visible = false;

		const bg2 = new PIXI.Graphics();
		bg2.beginFill(0x00ff00);
		bg2.drawRect(0, 0, width, height);
		bg2.endFill();
		container2.addChild(bg2);

		const ninica2 = new PIXI.Sprite(this.app.loader.resources["Ninica2"].texture);
		ninica2.position.set(middleWidth, middleHeight);
		ninica2.anchor.set(0.5);
		ninica2.scale.set(0.25);
		container2.addChild(ninica2);

		const text2 = new PIXI.Text("Ninica 2", textStyle);
		text2.position.set(middleWidth, middleHeight);
		text2.anchor.set(0.5);
		container2.addChild(text2);

		this.mainContainer.addChild(container2);

		// Container 3
		const container3 = new PIXI.Container();
		container3.visible = false;

		const bg3 = new PIXI.Graphics();
		bg3.beginFill(0x0000ff);
		bg3.drawRect(0, 0, width, height);
		bg3.endFill();
		container3.addChild(bg3);

		const ninica3 = new PIXI.Sprite(this.app.loader.resources["Ninica3"].texture);
		ninica3.position.set(middleWidth, middleHeight);
		ninica3.anchor.set(0.5);
		ninica3.scale.set(0.25);
		container3.addChild(ninica3);

		const text3 = new PIXI.Text("Ninica 3", textStyle);
		text3.position.set(middleWidth, middleHeight);
		text3.anchor.set(0.5);
		container3.addChild(text3);

		this.mainContainer.addChild(container3);

		// Buttons
		document.getElementById("container1Btn").addEventListener("click", () => {
			container1.visible = true;
			container2.visible = false;
			container3.visible = false;
		});

		document.getElementById("container2Btn").addEventListener("click", () => {
			container1.visible = false;
			container2.visible = true;
			container3.visible = false;
		});

		document.getElementById("container3Btn").addEventListener("click", () => {
			container1.visible = false;
			container2.visible = false;
			container3.visible = true;
		});
	}

	update() {
		this.stats.update();
	}
}

const game = new Game();
