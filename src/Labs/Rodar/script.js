import Assets from "../../Assets.js";

const image = Assets.Images.Ninica.Ninica1;
const StatusFPSs = document.getElementById("StatusFPSs");
let app = new PIXI.Application(500, 500, { antialias: true, transparent: false, resolution: 1 });
const stage = new PIXI.Container();
let sprite;

app.renderer.backgroundColor = 0xffff00;
document.body.appendChild(app.view);

PIXI.loader.add("Ninica", image).load(setup);

function setup() {
	sprite = new PIXI.Sprite(PIXI.loader.resources["Ninica"].texture);
	sprite.width = 300;
	sprite.height = 450;
	sprite.x = 250;
	sprite.y = 250;
	sprite.anchor.set(0.5, 0.5);
	app.stage.addChild(sprite);
	update();
}

function update() {

	sprite.rotation += 0.01;

	StatusFPSs.innerHTML = Number(PIXI.ticker.shared.FPS).toFixed(2)
	requestAnimationFrame(update);
}
