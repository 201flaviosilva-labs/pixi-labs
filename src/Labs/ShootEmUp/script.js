import Assets from "../../Assets.js";
import { randomNumber } from "../../utils.js";

class Enemy {
	constructor(app, mainContainer, x, y) {
		this.sprite = new PIXI.Sprite.from(app.loader.resources.Enemy.texture);
		this.sprite.anchor.set(0.5);
		this.sprite.x = x || randomNumber(0, 400);
		this.sprite.y = y || 0;
		this.life = 3;
		this.speed = 1;

		this.app = app;
		this.mainContainer = mainContainer;

		mainContainer.addChild(this.sprite);
	}

	hitted() {
		this.life--;
		this.sprite.alpha = 0.333 * this.life;
		if (this.life <= 0) this.destroy();
	}

	update() {
		this.sprite.y += this.speed;
		if (this.sprite.y > 800) this.destroy();
	}

	destroy() {
		this.mainContainer.removeChild(this.sprite);
	}
}

class Game {
	constructor() {
		console.clear();
		this.configs = {
			width: 400,
			height: 800,
			// resolution: window.devicePixelRatio || 1,
			antialias: true,
		};
		this.app = new PIXI.Application(this.configs);
		this.mainContainer = new PIXI.Container();
		this.app.stage.addChild(this.mainContainer);

		this.domGameContainer = document.getElementById("gameContainer");
		this.domGameContainer.appendChild(this.app.view);
		this.setup();
	}

	setup() {
		this.createStats();
		this.init();
		this.preload();
	}
	loadPreload() {
		this.create();
		this.app.ticker.add(this.update.bind(this));
	}

	createStats() {
		this.stats = new Stats();
		this.stats.showPanel(0); // 0: fps, 1: ms, 2: mb, 3+: custom
		this.stats.domElement.style.position = "fixed";
		this.stats.domElement.style.left = "0px";
		this.stats.domElement.style.top = "0px";
		document.body.appendChild(this.stats.domElement);
	}

	init() {
		this.configs.middleWidth = this.configs.width / 2;
		this.configs.middleHeight = this.configs.height / 2;

		this.app.stage.interactive = true;

		this.score = 0;

		this.bullets = [];
		this.bulletSpeed = 10;

		this.enemies = [];
		this.enemiesSpeed = 1;
	}

	preload() {
		const { Amarelo, Verde, Vermelho } = Assets.Sprites.Quadrados;

		this.app.loader
			.add("Player", Verde)
			.add("Enemy", Vermelho)
			.add("Shoot", Assets.Sprites.Disparo.Flame);

		this.app.loader.onComplete.add((e) => this.loadPreload());
		this.app.loader.load();
	}

	create() {
		const { width, height, middleWidth, middleHeight } = this.configs;
		// Player
		this.player = new PIXI.Sprite.from(this.app.loader.resources.Player.texture);
		this.player.anchor.set(0.5);
		this.player.position.set(middleWidth, middleHeight);
		this.mainContainer.addChild(this.player);
		this.app.stage.on("pointermove", e => this.player.position.set(e.data.global.x, e.data.global.y));
		this.domGameContainer.addEventListener("pointerup", e => this.createShoot());
		window.addEventListener("keyup", e => { if (e.keyCode === 32) this.createShoot(); });

		// Label Points
		this.labelPoints = new PIXI.Text("Pontos: " + this.score, { fontFamily: "Arial", fontSize: 24, fill: 0xffffff, align: "center" });
		this.labelPoints.anchor.set(0.5);
		this.labelPoints.position.set(middleWidth, middleHeight);
		this.mainContainer.addChild(this.labelPoints);

		setInterval(() => this.createEnemy(), 1000);
	}

	createEnemy() {
		const enemy = new Enemy(this.app, this.mainContainer, randomNumber(0, this.configs.width), 0);
		this.enemies.push(enemy);
	}

	createShoot() {
		const bullet = new PIXI.Sprite.from(this.app.loader.resources["Shoot"].texture);
		bullet.anchor.set(0.5);
		bullet.scale.set(0.5);
		bullet.position.set(this.player.x, this.player.y);
		this.mainContainer.addChild(bullet);
		this.bullets.push(bullet);
	}

	update() {
		this.moveEnemies();
		this.moveBullets();

		this.checkBulletsEnemiesCollision();

		this.stats.update();
	}

	moveEnemies() {
		this.enemies.forEach((enemy, i) => {
			enemy.update();
			if (enemy.sprite.y > this.configs.height) {
				this.enemies.splice(i, 1);
			}
		});
	}

	moveBullets() {
		this.bullets.forEach((bullet, i) => {
			bullet.y -= this.bulletSpeed;
			if (bullet.y < 0) {
				this.mainContainer.removeChild(bullet);
				this.bullets.splice(i, 1);
			}
		});
	}

	checkBulletsEnemiesCollision() {
		this.bullets.forEach((bullet, i) => {
			this.enemies.forEach((enemy, j) => {
				if (this.checkRectangleCollision(bullet.getBounds(), enemy.sprite.getBounds())) {
					this.mainContainer.removeChild(bullet);
					enemy.hitted();
					this.bullets.splice(i, 1);

					if (enemy.life <= 0) this.enemies.splice(j, 1);

					this.score++;
					this.labelPoints.text = "Pontos: " + this.score;
				}
			});
		});
	}

	checkRectangleCollision(r1, r2) {
		return !(
			r2.x > r1.x + r1.width ||
			r2.x + r2.width < r1.x ||
			r2.y > r1.y + r1.height ||
			r2.y + r2.height < r1.y
		);
	}
}

const game = new Game();
