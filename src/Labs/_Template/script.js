import Assets from "../../Assets.js";

class Game {
	constructor() {
		console.clear();
		this.configs = {
			width: 500,
			height: 500,
			antialias: true,
		};
		this.app = new PIXI.Application(this.configs);
		this.mainContainer = new PIXI.Container();
		this.app.stage.addChild(this.mainContainer);

		document.body.appendChild(this.app.view);

		this.setup();
	}

	setup() {
		this.createStats();
		this.init();
		this.preload();
	}

	posPreload() {
		this.create();
		this.app.ticker.add(this.update.bind(this));
	}

	createStats() {
		this.stats = new Stats();
		this.stats.showPanel(0);
		this.stats.domElement.style.position = "fixed";
		this.stats.domElement.style.left = "0px";
		this.stats.domElement.style.top = "0px";
		document.body.appendChild(this.stats.domElement);
	}

	init() {
		this.configs.middleWidth = this.configs.width / 2;
		this.configs.middleHeight = this.configs.height / 2;
	}

	preload() {
		this.app.loader
			.add("Ninica1", Assets.Images.Ninica.Ninica1)
			.add("Ninica2", Assets.Images.Ninica.Ninica2);

		this.app.loader.onComplete.add((e) => this.posPreload());

		this.app.loader.load();
	}

	create() {
		const { width, height, middleWidth, middleHeight } = this.configs;
		const ninica = new PIXI.Sprite(this.app.loader.resources["Ninica1"].texture);
		ninica.position.set(middleWidth, middleHeight);
		ninica.anchor.set(0.5);
		ninica.scale.set(0.25);
		this.mainContainer.addChild(ninica);
	}

	update() {
		this.stats.update();
	}
}

const game = new Game();
